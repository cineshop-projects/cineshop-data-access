module gitlab.com/cineshop-projects/cineshop-data-access

go 1.14

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.10
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.6.0
	go.mongodb.org/mongo-driver v1.4.1
)
