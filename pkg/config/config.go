package config

import (
	"os"
	"strconv"
	"strings"
)

const (
	mockRedisKey      = "CINESHOP_MOCK_REDIS"
	mockPostgreSQLKey = "CINESHOP_MOCK_POSTGRESQL"
	mockMongoDBKey    = "CINESHOP_MOCK_MONGODB"

	redisHostKey     = "CINESHOP_REDIS_HOST"
	redisPortKey     = "CINESHOP_REDIS_PORT"
	redisPasswordKey = "CINESHOP_REDIS_PASSWORD"

	postgreSQLHostKey     = "CINESHOP_POSTGRESQL_HOST"
	postgreSQLPortKey     = "CINESHOP_POSTGRESQL_PORT"
	postgreSQLUserKey     = "CINESHOP_POSTGRESQL_USER"
	postgreSQLPasswordKey = "CINESHOP_POSTGRESQL_PASSWORD"
	postgreSQLDBNameKey   = "CINESHOP_POSTGRESQL_DBNAME"

	mongoDBHostKey     = "CINESHOP_MONGODB_HOST"
	mongoDBPortKey     = "CINESHOP_MONGODB_PORT"
	mongoDBPasswordKey = "CINESHOP_MONGODB_PASSWORD"
)

type Config struct {
	Mock struct {
		Redis      bool
		PostgreSQL bool
		MongoDB    bool
	}
	Redis struct {
		Host     string
		Port     int
		Password string
	}
	PostgreSQL struct {
		Host     string
		Port     int
		User     string
		Password string
		DBName   string
	}
	MongoDB struct {
		Host     string
		Port     int
		Password string
	}
}

var config Config

func init() {
	mockConfig()
	redisConfig()
	postgreSQLConfig()
	mongoDBConfig()
}

func GetConfig() Config {
	return config
}

func mockConfig() {
	// Mock by default
	config.Mock.Redis = !strings.EqualFold(os.Getenv(mockRedisKey), "false")
	config.Mock.PostgreSQL = !strings.EqualFold(os.Getenv(mockPostgreSQLKey), "false")
	config.Mock.MongoDB = !strings.EqualFold(os.Getenv(mockMongoDBKey), "false")
}

func redisConfig() {
	config.Redis.Host = getString(redisHostKey, "localhost")
	config.Redis.Port = getInt(redisPortKey, 6379)
	config.Redis.Password = getString(redisPasswordKey, "")
}

func postgreSQLConfig() {
	config.PostgreSQL.Host = getString(postgreSQLHostKey, "localhost")
	config.PostgreSQL.Port = getInt(postgreSQLPortKey, 5432)
	config.PostgreSQL.User = getString(postgreSQLUserKey, "postgres")
	config.PostgreSQL.Password = getString(postgreSQLPasswordKey, "password")
	config.PostgreSQL.DBName = getString(postgreSQLDBNameKey, "postgres")
}

func mongoDBConfig() {
	config.MongoDB.Host = getString(mongoDBHostKey, "localhost")
	config.MongoDB.Port = getInt(mongoDBPortKey, 27017)
	config.MongoDB.Password = getString(mongoDBPasswordKey, "")
}

func getString(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}

func getInt(key string, defaultValue int) int {
	if value, ok := strconv.Atoi(os.Getenv(key)); ok == nil {
		return value
	}
	return defaultValue
}
