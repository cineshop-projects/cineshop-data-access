package impl

import (
	"time"

	log "github.com/sirupsen/logrus"

	_ "github.com/jackc/pgx/v4"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type paymentModel struct {
	gorm.Model
	TransactionId string
	Timestamp     time.Time
	UserId        model.UserId
	Cart          string
	OrderDetails  string
	Status        model.TransactionStatus
}

type PaymentDaoSql struct {
	client *gorm.DB
	logger *log.Entry
}

func NewPaymentDaoSql(client *gorm.DB) *PaymentDaoSql {
	if client == nil {
		panic("Failed to instantiate Payment DAO")
	}

	client.AutoMigrate(&paymentModel{})

	return &PaymentDaoSql{
		client: client,
		logger: log.WithField("Context", "PaymentDaoSql"),
	}
}

func (sdao PaymentDaoSql) Insert(payment model.Payment) (model.Payment, error) {
	logger := sdao.logger.WithField("Function", "Insert")
	logger.Debug()

	dbPayment := paymentModel{
		TransactionId: payment.TransactionId,
		Timestamp:     payment.Timestamp,
		UserId:        payment.UserId,
		Cart:          payment.Cart,
		OrderDetails:  payment.OrderDetails,
		Status:        payment.Status,
	}

	ret := sdao.client.Create(&dbPayment)
	if ret.Error != nil {
		logger.Errorf("Failed to insert payment: %v", ret.Error)
		return model.Payment{}, ret.Error
	}

	retDbPayment, ok := ret.Value.(*paymentModel)
	if !ok {
		logger.Error("Failed to retrieve inserted payment")
		return model.Payment{}, model.EntityInternalError
	}

	retPayment := model.Payment{
		Id:            int32(retDbPayment.ID),
		TransactionId: retDbPayment.TransactionId,
		Timestamp:     retDbPayment.Timestamp,
		UserId:        retDbPayment.UserId,
		Cart:          retDbPayment.Cart,
		OrderDetails:  retDbPayment.OrderDetails,
		Status:        retDbPayment.Status,
	}
	logger.Debug("Return payment")
	return retPayment, nil
}
