package impl

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/go-redis/redis/v8"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

const (
	tokenIdPrefix   = "token:"
	tokenExpiration = 2 * time.Hour
)

type TokenDaoRedis struct {
	client *redis.Client
	logger *log.Entry
}

func NewTokenDaoRedis(client *redis.Client) *TokenDaoRedis {
	if client == nil {
		panic("Failed to instantiate Token DAO")
	}

	return &TokenDaoRedis{
		client: client,
		logger: log.WithField("Context", "TokenDaoRedis"),
	}
}

func (rdao TokenDaoRedis) Exists(token model.Token) (bool, error) {
	logger := rdao.logger.WithField("Function", "Exists")
	logger.Debug()

	result, err := rdao.client.Exists(context.Background(), tokenIdPrefix+string(token)).Result()
	if err != nil {
		logger.Errorf("Failed to check whether token exists: %v", err)
		return false, err
	}

	exists := result > 0

	logger.Debugf("Return %v", exists)
	return exists, nil
}

func (rdao TokenDaoRedis) Insert(token model.Token) error {
	logger := rdao.logger.WithField("Function", "Insert")
	logger.Debug()

	err := rdao.client.Set(context.Background(), tokenIdPrefix+string(token), "", tokenExpiration).Err()
	if err != nil {
		logger.Errorf("Failed to insert token: %v", err)
		return err
	}

	logger.Debug("Return nil error")
	return nil
}

func (rdao *TokenDaoRedis) Delete(token model.Token) error {
	logger := rdao.logger.WithField("Function", "Delete")
	logger.Debug()

	result, err := rdao.client.Del(context.Background(), tokenIdPrefix+string(token)).Result()
	if err != nil {
		logger.Errorf("Failed to delete token: %v", err)
		return err
	}

	if result == 0 {
		logger.Debug("Token does not exist")
		return model.EntityNotFound
	}

	logger.Debug("Return nil error")
	return nil
}
