package impl

import (
	"context"
	"sort"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ProductDaoMongo struct {
	collection *mongo.Collection
	logger     *log.Entry
}

func NewProductDaoMongo(collection *mongo.Collection) *ProductDaoMongo {
	if collection == nil {
		panic("Failed to instantiate Product DAO")
	}

	return &ProductDaoMongo{
		collection: collection,
		logger:     log.WithField("Context", "ProductDaoMongo"),
	}
}

func (mdao *ProductDaoMongo) GetAll(order model.Order) ([]model.Product, error) {
	logger := mdao.logger.WithField("Function", "GetAll")
	logger.Debug()

	var products []model.Product

	ctx := context.Background()

	findOptions := options.Find()
	if order == model.OrderAsc {
		findOptions.SetSort(bson.D{{Key: "label", Value: 1}})
	} else if order == model.OrderDesc {
		findOptions.SetSort(bson.D{{Key: "label", Value: -1}})
	}

	cur, err := mdao.collection.Find(ctx, bson.D{{}}, findOptions)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			logger.Debug("Empty list of products")
			return nil, nil
		}
		logger.Errorf("Failed to retrieve list of products: %v", err)
		return nil, err
	}

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var product model.Product
		err := cur.Decode(&product)
		if err != nil {
			logger.Errorf("Failed to unmarshal product: %v", err)
			return nil, err
		}

		products = append(products, product)
	}

	if err := cur.Err(); err != nil {
		logger.Errorf("Failed to iterate through list of products: %v", err)
		return nil, err
	}

	if order != model.OrderAsc && order != model.OrderDesc {
		sort.Slice(products, func(first, second int) bool {
			return products[first].Id < products[second].Id
		})
	}

	logger.Debug("Return list of products")
	return products, nil
}

func (mdao *ProductDaoMongo) GetById(id int32) (model.Product, error) {
	logger := mdao.logger.WithField("Function", "GetById")
	logger.Debug()

	var product model.Product

	err := mdao.collection.FindOne(context.Background(), bson.D{{Key: "_id", Value: id}}).Decode(&product)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			logger.Debugf("Product %v not found", id)
			return model.Product{}, model.EntityNotFound
		}
		logger.Errorf("Failed to retrieve product %v: %v", id, err)
		return model.Product{}, err
	}

	logger.Debugf("Return product %v", id)
	return product, nil
}

func (mdao *ProductDaoMongo) Insert(product model.Product) (model.Product, error) {
	logger := mdao.logger.WithField("Function", "Insert")
	logger.Debug()

	result, err := mdao.collection.InsertOne(context.Background(), product)
	if err != nil {
		logger.Errorf("Failed to insert product: %v", err)
		return model.Product{}, err
	}

	id, ok := result.InsertedID.(int32)
	if !ok {
		logger.Error("Failed to retrieve id of inserted product")
		return model.Product{}, model.EntityInternalError
	}

	product.Id = id
	logger.Debugf("Return product %v", id)
	return product, nil
}
