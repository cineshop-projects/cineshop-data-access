package impl

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"

	log "github.com/sirupsen/logrus"
)

const (
	cartIdPrefix   = "cart:"
	cartExpiration = 24 * time.Hour
)

type CartDaoRedis struct {
	client *redis.Client
	logger *log.Entry
}

func NewCartDaoRedis(client *redis.Client) *CartDaoRedis {
	if client == nil {
		panic("Failed to instantiate Cart DAO")
	}

	return &CartDaoRedis{
		client: client,
		logger: log.WithField("Context", "CartDaoRedis"),
	}
}

func (rdao CartDaoRedis) GetByUserId(userId model.UserId) (model.Cart, error) {
	logger := rdao.logger.WithField("Function", "GetByUserId")
	logger.Debug()

	jsonCart, err := rdao.client.Get(context.Background(), cartIdPrefix+string(userId)).Result()
	if err != nil {
		if err == redis.Nil {
			logger.Debug("Empty cart")
			return model.Cart{LastUpdate: time.Now()}, nil
		}
		logger.Errorf("Failed to retrieve cart: %v", err)
		return model.Cart{}, err
	}

	var cart model.Cart
	err = json.Unmarshal([]byte(jsonCart), &cart)
	if err != nil {
		logger.Errorf("Failed to unmarshal cart: %v", err)
		return model.Cart{}, err
	}

	logger.Debug("Return cart")
	return cart, nil
}

func (rdao CartDaoRedis) Upsert(userId model.UserId, cart model.Cart) (model.Cart, error) {
	logger := rdao.logger.WithField("Function", "Upsert")
	logger.Debug()

	jsonCart, err := json.Marshal(cart)
	if err != nil {
		logger.Errorf("Failed to marshal cart: %v", err)
		return model.Cart{}, err
	}

	err = rdao.client.Set(context.Background(), cartIdPrefix+string(userId), jsonCart, cartExpiration).Err()
	if err != nil {
		logger.Errorf("Failed to upsert cart: %v", err)
		return model.Cart{}, err
	}

	logger.Debug("Return cart")
	return cart, nil
}
