package impl

import (
	"context"
	"fmt"
	"log"
	"strings"
	"sync"

	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"

	"github.com/go-redis/redis/v8"
	_ "github.com/jackc/pgx/v4"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	redisClient *redis.Client
	pgClient    *gorm.DB
	mongoClient *mongo.Client

	redisMux sync.Mutex
	pgMux    sync.Mutex
	mongoMux sync.Mutex
)

func init() {
	cfg := config.GetConfig()

	if cfg.Mock.Redis {
		log.Printf("Mock Redis data source")
	} else {
		go GetRedisClient()
	}

	if cfg.Mock.PostgreSQL {
		log.Printf("Mock PostgreSQL data source")
	} else {
		go GetPostgreSqlClient()
	}

	if cfg.Mock.MongoDB {
		log.Printf("Mock MongoDB data source")
	} else {
		go GetMongoDbClient()
	}
}

func GetRedisClient() *redis.Client {
	redisMux.Lock()
	defer redisMux.Unlock()

	if redisClient != nil {
		return redisClient
	}

	cfg := config.GetConfig()

	redisClient = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%v:%v", cfg.Redis.Host, cfg.Redis.Port),
		Password: cfg.Redis.Password,
	})
	if err := redisClient.Ping(context.Background()).Err(); err != nil {
		log.Printf("Failed to connect to redis: %v", err)
		return nil
	}

	log.Printf("Redis connection successful")
	return redisClient
}

func GetPostgreSqlClient() *gorm.DB {
	pgMux.Lock()
	defer pgMux.Unlock()

	if pgClient != nil {
		return pgClient
	}

	cfg := config.GetConfig()

	connectionStr := fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=disable",
		cfg.PostgreSQL.Host, cfg.PostgreSQL.Port, cfg.PostgreSQL.User, cfg.PostgreSQL.Password, cfg.PostgreSQL.DBName)
	pgClient, err := gorm.Open("postgres", connectionStr)
	if err != nil {
		log.Printf("Failed to connect to postgresql: %v", err)
		return nil
	}

	log.Printf("PostgreSQL connection successful")
	return pgClient
}

func GetMongoDbClient() *mongo.Client {
	mongoMux.Lock()
	defer mongoMux.Unlock()

	if mongoClient != nil {
		return mongoClient
	}

	cfg := config.GetConfig()

	var connectionUri string
	if len(cfg.MongoDB.Password) == 0 {
		connectionUri = fmt.Sprintf("mongodb://admin@%v:%v/", cfg.MongoDB.Host, cfg.MongoDB.Port)
	} else {
		connectionUri = fmt.Sprintf("mongodb://admin:%v@%v:%v/", cfg.MongoDB.Password, cfg.MongoDB.Host, cfg.MongoDB.Port)
	}
	mongoClientOptions := options.Client().ApplyURI(connectionUri)
	mongoClient, err := mongo.Connect(context.Background(), mongoClientOptions)
	if err != nil {
		log.Printf("Failed to connect to mongodb: %v", err)
		return nil
	}

	if err = mongoClient.Ping(context.Background(), nil); err != nil {
		log.Printf("Failed to connect to mongodb: %v", err)
		mongoClient = nil
		return nil
	}

	log.Printf("MongoDB connection successful")
	return mongoClient
}

func Cleanup() error {
	var err []string

	cfg := config.GetConfig().Mock

	if !cfg.Redis {
		redisErr := redisClient.Close()
		if redisErr != nil {
			err = append(err, fmt.Sprintf("failed to close redis connection: %v", redisErr))
		} else {
			log.Printf("Redis disconnection successful")
		}
	}

	if !cfg.PostgreSQL {
		pgErr := pgClient.Close()
		if pgErr != nil {
			err = append(err, fmt.Sprintf("failed to close postgresql connection: %v", pgErr))
		} else {
			log.Printf("PostgreSQL disconnection successful")
		}
	}

	if !cfg.MongoDB {
		mongoErr := mongoClient.Disconnect(context.Background())
		if mongoErr != nil {
			err = append(err, fmt.Sprintf("failed to close mongodb connection: %v", mongoErr))
		} else {
			log.Printf("MongoDB disconnection successful")
		}
	}

	if err != nil {
		return fmt.Errorf("failed to close one or several connections: %v", strings.Join(err, "\n"))
	}
	return nil
}
