package impl

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/go-redis/redis/v8"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

const (
	couponIdPrefix   = "coupon:"
	couponExpiration = 10 * time.Second
)

type CouponDaoRedis struct {
	client *redis.Client
	logger *log.Entry
}

func NewCouponDaoRedis(client *redis.Client) *CouponDaoRedis {
	if client == nil {
		panic("Failed to instantiate Coupon DAO")
	}

	return &CouponDaoRedis{
		client: client,
		logger: log.WithField("Context", "CouponDaoRedis"),
	}
}

func (rdao CouponDaoRedis) Exists(coupon model.Coupon) (bool, error) {
	logger := rdao.logger.WithField("Function", "Exists")
	logger.Debug()

	result, err := rdao.client.Exists(context.Background(), couponIdPrefix+string(coupon)).Result()
	if err != nil {
		logger.Errorf("Failed to check whether coupon exists: %v", err)
		return false, err
	}

	exists := result > 0

	logger.Debugf("Return %v", exists)
	return exists, nil
}

func (rdao CouponDaoRedis) Insert(coupon model.Coupon) error {
	logger := rdao.logger.WithField("Function", "Insert")
	logger.Debug()

	err := rdao.client.Set(context.Background(), couponIdPrefix+string(coupon), "", couponExpiration).Err()
	if err != nil {
		logger.Errorf("Failed to insert coupon: %v", err)
		return err
	}

	logger.Debug("Return nil error")
	return nil
}

func (rdao *CouponDaoRedis) Delete(coupon model.Coupon) error {
	logger := rdao.logger.WithField("Function", "Delete")
	logger.Debug()

	_, err := rdao.client.Del(context.Background(), couponIdPrefix+string(coupon)).Result()
	if err != nil {
		logger.Errorf("Failed to delete coupon: %v", err)
		return err
	}

	logger.Debug("Return nil error")
	return nil
}
