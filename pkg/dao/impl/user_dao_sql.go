package impl

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type userModel struct {
	gorm.Model
	FirstName string
	LastName  string
	Email     string
	Password  string
}

type UserDaoSql struct {
	client *gorm.DB
	logger *log.Entry
}

func NewUserDaoSql(client *gorm.DB) *UserDaoSql {
	if client == nil {
		panic("Failed to instantiate User DAO")
	}

	client.AutoMigrate(&userModel{})

	return &UserDaoSql{
		client: client,
		logger: log.WithField("Context", "UserDaoSql"),
	}
}

func (sdao *UserDaoSql) GetByEmail(email string) (model.User, error) {
	logger := sdao.logger.WithField("Function", "GetByEmail")
	logger.Debug()

	var dbUser userModel
	ret := sdao.client.Where("email = ?", email).First(&dbUser)
	if ret.Error != nil {
		if ret.Error == gorm.ErrRecordNotFound {
			logger.Debugf("User %v not found", email)
			return model.User{}, model.EntityNotFound
		}
		logger.Errorf("Failed to retrieve user %v: %v", email, ret.Error)
		return model.User{}, ret.Error
	}

	return sdao.dbToDao(ret.Value)
}

func (sdao *UserDaoSql) Insert(user model.User) (model.User, error) {
	logger := sdao.logger.WithField("Function", "Insert")
	logger.Debug()

	_, err := sdao.GetByEmail(user.Email)
	if err == nil {
		logger.Errorf("Failed to insert user %v: user already exists", user.Email)
		return model.User{}, model.EntityConflict
	}
	if err != model.EntityNotFound {
		logger.Errorf("Failed to check whether user %v exists: %v", user.Email, err)
		return model.User{}, err
	}

	dbUser := userModel{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  user.Password,
	}

	ret := sdao.client.Create(&dbUser)
	if ret.Error != nil {
		logger.Errorf("Failed to insert user %v: %v", user.Email, ret.Error)
		return model.User{}, ret.Error
	}

	return sdao.dbToDao(ret.Value)
}

func (sdao *UserDaoSql) dbToDao(ifUser interface{}) (model.User, error) {
	logger := sdao.logger.WithField("Function", "dbToDao")
	logger.Debug()

	dbUser, ok := ifUser.(*userModel)
	if !ok {
		logger.Error("Failed to convert user model")
		return model.User{}, model.EntityInternalError
	}

	user := model.User{
		Id:        fmt.Sprintf("%v", dbUser.ID),
		FirstName: dbUser.FirstName,
		LastName:  dbUser.LastName,
		Email:     dbUser.Email,
		Password:  dbUser.Password,
	}

	logger.Debugf("Return user %v", user.Email)
	return user, nil
}
