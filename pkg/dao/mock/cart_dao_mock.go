package mock

import (
	"time"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type CartDaoMock struct {
	carts map[model.UserId]model.Cart
}

func NewCartDaoMock() *CartDaoMock {
	return &CartDaoMock{
		carts: make(map[model.UserId]model.Cart),
	}
}

func (mdao *CartDaoMock) GetByUserId(userId model.UserId) (model.Cart, error) {
	if cart, exist := mdao.carts[userId]; exist {
		return cart, nil
	}
	return model.Cart{LastUpdate: time.Now()}, nil
}

func (mdao *CartDaoMock) Upsert(userId model.UserId, cart model.Cart) (model.Cart, error) {
	mdao.carts[userId] = cart
	return cart, nil
}
