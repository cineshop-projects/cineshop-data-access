package mock

import (
	"time"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type TokenDaoMock struct {
	tokens map[model.Token]time.Time
}

func NewTokenDaoMock() *TokenDaoMock {
	return &TokenDaoMock{
		tokens: make(map[model.Token]time.Time),
	}
}

func (mdao *TokenDaoMock) Exists(token model.Token) (bool, error) {
	expiryDate, exist := mdao.tokens[token]
	if exist {
		if !expiryDate.After(time.Now()) {
			delete(mdao.tokens, token)
			exist = false
		}
	}
	return exist, nil
}

func (mdao *TokenDaoMock) Insert(token model.Token) error {
	if expiryDate, exist := mdao.tokens[token]; exist {
		if !expiryDate.After(time.Now()) {
			delete(mdao.tokens, token)
		} else {
			return model.EntityConflict
		}
	}
	expiryDate := time.Now().Add(3 * time.Minute)
	mdao.tokens[token] = expiryDate
	return nil
}

func (mdao *TokenDaoMock) Delete(token model.Token) error {
	if _, exist := mdao.tokens[token]; exist {
		delete(mdao.tokens, token)
		return nil
	}
	return model.EntityNotFound
}
