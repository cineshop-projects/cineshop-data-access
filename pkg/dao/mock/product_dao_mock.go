package mock

import (
	"sort"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type ProductDaoMock struct {
	products map[int32]model.Product
}

func NewProductDaoMock() *ProductDaoMock {
	return &ProductDaoMock{
		products: make(map[int32]model.Product),
	}
}

func (mdao *ProductDaoMock) GetAll(order model.Order) ([]model.Product, error) {
	var products []model.Product
	for _, product := range mdao.products {
		products = append(products, product)
	}

	if len(products) == 0 {
		return nil, nil
	}

	if order == model.OrderAsc {
		sort.Slice(products, func(i, j int) bool {
			return products[i].Label < products[j].Label
		})
	} else if order == model.OrderDesc {
		sort.Slice(products, func(i, j int) bool {
			return products[i].Label > products[j].Label
		})
	} else {
		sort.Slice(products, func(first, second int) bool {
			return products[first].Id < products[second].Id
		})
	}

	return products, nil
}

func (mdao *ProductDaoMock) GetById(id int32) (model.Product, error) {
	product, exist := mdao.products[id]
	if exist {
		return product, nil
	}
	return model.Product{}, model.EntityNotFound
}

func (mdao *ProductDaoMock) Insert(product model.Product) (model.Product, error) {
	if _, exist := mdao.products[product.Id]; exist {
		return model.Product{}, model.EntityConflict
	}
	mdao.products[product.Id] = product
	return product, nil
}
