package mock

import (
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type PaymentDaoMock struct {
	users map[int32]model.Payment
}

func NewPaymentDaoMock() *PaymentDaoMock {
	return &PaymentDaoMock{
		users: make(map[int32]model.Payment),
	}
}

func (mdao *PaymentDaoMock) Insert(payment model.Payment) (model.Payment, error) {
	payment.Id = model.GenerateInt32Id()
	mdao.users[payment.Id] = payment
	return payment, nil
}
