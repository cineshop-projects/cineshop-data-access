package mock

import (
	"time"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type CouponDaoMock struct {
	coupons map[model.Coupon]time.Time
}

func NewCouponDaoMock() *CouponDaoMock {
	return &CouponDaoMock{
		coupons: make(map[model.Coupon]time.Time),
	}
}

func (mdao *CouponDaoMock) Exists(coupon model.Coupon) (bool, error) {
	expiryDate, exist := mdao.coupons[coupon]
	if exist {
		if !expiryDate.After(time.Now()) {
			delete(mdao.coupons, coupon)
			exist = false
		}
	}
	return exist, nil
}

func (mdao *CouponDaoMock) Insert(coupon model.Coupon) error {
	if expiryDate, exist := mdao.coupons[coupon]; exist {
		if !expiryDate.After(time.Now()) {
			delete(mdao.coupons, coupon)
		} else {
			return model.EntityConflict
		}
	}
	expiryDate := time.Now().Add(3 * time.Minute)
	mdao.coupons[coupon] = expiryDate
	return nil
}

func (mdao *CouponDaoMock) Delete(coupon model.Coupon) error {
	if _, exist := mdao.coupons[coupon]; exist {
		delete(mdao.coupons, coupon)
		return nil
	}
	return model.EntityNotFound
}
