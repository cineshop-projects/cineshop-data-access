package mock

import (
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type UserDaoMock struct {
	users map[string]model.User
}

func NewUserDaoMock() *UserDaoMock {
	return &UserDaoMock{
		users: make(map[string]model.User),
	}
}

func (mdao *UserDaoMock) GetByEmail(email string) (model.User, error) {
	if user, exist := mdao.users[email]; exist {
		return user, nil
	}
	return model.User{}, model.EntityNotFound
}

func (mdao *UserDaoMock) Insert(user model.User) (model.User, error) {
	if _, exist := mdao.users[user.Email]; exist {
		return model.User{}, model.EntityConflict
	}
	user.Id = user.Email
	mdao.users[user.Email] = user
	return user, nil
}
