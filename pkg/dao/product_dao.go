package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type ProductDao interface {
	GetAll(model.Order) ([]model.Product, error)
	GetById(int32) (model.Product, error)
	Insert(model.Product) (model.Product, error)
}

var (
	productDao          ProductDao
	retryProductDaoInit bool

	productDaoMux sync.Mutex
)

func init() {
	go GetProductDao()
}

func GetProductDao() ProductDao {
	productDaoMux.Lock()
	defer productDaoMux.Unlock()

	if productDao != nil && !retryProductDaoInit {
		return productDao
	}
	if config.GetConfig().Mock.Redis {
		productDao = mock.NewProductDaoMock()
	} else {
		client := impl.GetMongoDbClient()
		if client != nil {
			productDao = impl.NewProductDaoMongo(client.Database("cineshop").Collection("products"))
		} else {
			log.Printf("Waning: Fallback to ProductDaoMock")
			productDao = mock.NewProductDaoMock()
			retryProductDaoInit = true
		}
	}
	return productDao
}
