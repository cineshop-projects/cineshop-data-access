package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type CouponDao interface {
	Exists(model.Coupon) (bool, error)
	Insert(model.Coupon) error
	Delete(model.Coupon) error
}

var (
	couponDao          CouponDao
	retryCouponDaoInit bool

	couponDaoMux sync.Mutex
)

func init() {
	go GetCouponDao()
}

func GetCouponDao() CouponDao {
	couponDaoMux.Lock()
	defer couponDaoMux.Unlock()

	if couponDao != nil && !retryCouponDaoInit {
		return couponDao
	}
	if config.GetConfig().Mock.Redis {
		couponDao = mock.NewCouponDaoMock()
	} else {
		client := impl.GetRedisClient()
		if client != nil {
			couponDao = impl.NewCouponDaoRedis(client)
		} else {
			log.Printf("Waning: Fallback to CouponDaoMock")
			couponDao = mock.NewCouponDaoMock()
			retryCouponDaoInit = true
		}
	}
	return couponDao
}
