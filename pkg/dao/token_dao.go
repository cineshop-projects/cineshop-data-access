package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type TokenDao interface {
	Exists(model.Token) (bool, error)
	Insert(model.Token) error
	Delete(model.Token) error
}

var (
	tokenDao          TokenDao
	retryTokenDaoInit bool

	tokenDaoMux sync.Mutex
)

func init() {
	go GetTokenDao()
}

func GetTokenDao() TokenDao {
	tokenDaoMux.Lock()
	defer tokenDaoMux.Unlock()

	if tokenDao != nil && !retryTokenDaoInit {
		return tokenDao
	}
	if config.GetConfig().Mock.Redis {
		tokenDao = mock.NewTokenDaoMock()
	} else {
		client := impl.GetRedisClient()
		if client != nil {
			tokenDao = impl.NewTokenDaoRedis(client)
		} else {
			log.Printf("Waning: Fallback to TokenDaoMock")
			tokenDao = mock.NewTokenDaoMock()
			retryTokenDaoInit = true
		}
	}
	return tokenDao
}
