package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type UserDao interface {
	GetByEmail(email string) (model.User, error)
	Insert(model.User) (model.User, error)
}

var (
	userDao          UserDao
	retryUserDaoInit bool

	userDaoMux sync.Mutex
)

func init() {
	go GetUserDao()
}

func GetUserDao() UserDao {
	userDaoMux.Lock()
	defer userDaoMux.Unlock()

	if userDao != nil && !retryUserDaoInit {
		return userDao
	}
	if config.GetConfig().Mock.Redis {
		userDao = mock.NewUserDaoMock()
	} else {
		client := impl.GetPostgreSqlClient()
		if client != nil {
			userDao = impl.NewUserDaoSql(client)
		} else {
			log.Printf("Waning: Fallback to UserDaoMock")
			userDao = mock.NewUserDaoMock()
			retryUserDaoInit = true
		}
	}
	return userDao
}
