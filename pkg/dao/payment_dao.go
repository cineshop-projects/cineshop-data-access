package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type PaymentDao interface {
	Insert(model.Payment) (model.Payment, error)
}

var (
	paymentDao          PaymentDao
	retryPaymentDaoInit bool

	paymentDaoMux sync.Mutex
)

func init() {
	go GetPaymentDao()
}

func GetPaymentDao() PaymentDao {
	paymentDaoMux.Lock()
	defer paymentDaoMux.Unlock()

	if paymentDao != nil && !retryPaymentDaoInit {
		return paymentDao
	}
	if config.GetConfig().Mock.Redis {
		paymentDao = mock.NewPaymentDaoMock()
	} else {
		client := impl.GetPostgreSqlClient()
		if client != nil {
			paymentDao = impl.NewPaymentDaoSql(client)
		} else {
			log.Printf("Waning: Fallback to PaymentDaoMock")
			paymentDao = mock.NewPaymentDaoMock()
			retryPaymentDaoInit = true
		}
	}
	return paymentDao
}
