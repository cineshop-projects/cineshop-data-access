package dao

import (
	"log"
	"sync"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/config"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/impl"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/mock"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

type CartDao interface {
	GetByUserId(model.UserId) (model.Cart, error)
	Upsert(model.UserId, model.Cart) (model.Cart, error)
}

var (
	cartDao          CartDao
	retryCartDaoInit bool

	cartDaoMux sync.Mutex
)

func init() {
	go GetCartDao()
}

func GetCartDao() CartDao {
	cartDaoMux.Lock()
	defer cartDaoMux.Unlock()

	if cartDao != nil && !retryCartDaoInit {
		return cartDao
	}
	if config.GetConfig().Mock.Redis {
		cartDao = mock.NewCartDaoMock()
		retryCartDaoInit = false
	} else {
		client := impl.GetRedisClient()
		if client != nil {
			cartDao = impl.NewCartDaoRedis(client)
			retryCartDaoInit = false
		} else {
			log.Printf("Waning: Fallback to CartDaoMock")
			cartDao = mock.NewCartDaoMock()
			retryCartDaoInit = true
		}
	}
	return cartDao
}
