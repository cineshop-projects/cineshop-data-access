package model

import "time"

type Payment struct {
	Id            int32
	TransactionId string
	Timestamp     time.Time
	UserId        UserId
	Cart          string
	OrderDetails  string
	Status        TransactionStatus
}
