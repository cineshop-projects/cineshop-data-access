package model

import (
	"fmt"
	"time"
)

const (
	OrderAsc  Order = "asc"
	OrderDesc Order = "desc"

	StatusApprove     TransactionStatus = "APPROVE"
	StatusSoftDecline TransactionStatus = "SOFT_DECLINE"
	StatusHardDecline TransactionStatus = "HARD_DECLINE"
)

type (
	Order             string
	Token             string
	UserId            string
	TransactionStatus string
	Coupon            string
)

var (
	EntityNotFound      = fmt.Errorf("not found")
	EntityConflict      = fmt.Errorf("conflict")
	EntityInternalError = fmt.Errorf("internal error")
)

func GenerateInt32Id() int32 {
	return int32((time.Now().UnixNano() - time.Time{}.AddDate(2019, 0, 0).UnixNano()) / int64(time.Millisecond))
}
