package model

type Product struct {
	Id          int32   `json:"id" bson:"_id"`
	Label       string  `json:"label" bson:"label"`
	Description string  `json:"description,omitempty" bson:"description,omitempty"`
	Image       string  `json:"image,omitempty" bson:"image,omitempty"`
	Price       float64 `json:"price" bson:"price"`
}
