package model

type User struct {
	Id        string
	FirstName string
	LastName  string
	Email     string
	Password  string
}
