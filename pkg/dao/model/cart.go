package model

import "time"

type Item struct {
	Product
	Quantity int32 `json:"quantity"`
}

type Items map[int32]Item

type Cart struct {
	Items      Items     `json:"items,omitempty"`
	Total      float64   `json:"total"`
	SaleTotal  float64   `json:"sale_total"`
	LastUpdate time.Time `json:"last_update"`
	Coupon     string    `json:"coupon,omitempty"`
	Locked     bool      `json:"-"`
}
